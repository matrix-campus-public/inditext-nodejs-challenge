# Introducción
Basado en el enunciado de Inditex para la prueba backend de JAVA he desarrollado este "spinoff" backend para NodeJS sobre las siguientes tecnologías base:
## Lenguaje y plataforma
- NodeJS version >= 14
- ExpressJS >= 4.17
- NestJS >= 9.0

## Librerías principales
- Prisma
- Date-fns
- Class validator

Se utiliza **Prisma** como gestor de migraciones de base de datos y ORM.

Se usa **Date-fns** para la gestión y normalización de datos temporales y fechas. Es una librería similar a moment, pero con unas firmas de métodos mucho más semánticas y entendibles, además de ser compatible con typescript de forma nativa.

Class Validator y reflect metadata para implementar validaciones directamente en las propiedades de clase.

## Arquitectura
Como ya se demostró una arquitectura hexagonal en la versión java de la prueba, en esta ocasión se muestra una arquitectura modular encapsulada.
Cada módulo dispone de su propio directorio y este a su vez encapsula todas las clases específicas en subpaquetes con responsabilidades cohesivos (con un criterio común). 

Si hubiese más lógica con diferentes módulos, cada módulo solo debería relacionarse con el core de la aplicación o con el módulo common y no directamente con otros módulos, así se potencia la portabilidad del módulo que es lo que queremos conseguir en esta ocasión.

Los puntos de entrada y salida de datos en cada módulo serán siempre los controllers.
 
En el siguiente diagrama se muestra un representación de como sería esta arquitectura con varios módulos y más desarrollada, en la que XModule representa cualquier posible paquete adicional. Common sería el paquete del core con el que se pueden conectar los módulos, sin embargo los módulos no se conectarán directamente entre ellos.
![Modules](/extras/packages_modules.png "Modules projection")

## Base de datos
Como motor de base de datos se usa SQLite v3.
El diseño del esquema de la base de datos se puede encontrar en:
*./prisma/schema.prisma*

Además hay un seeder para inyectar los datos iniciales en el archivo *./prisma/seed.ts*

## Comandos de instalación

En primer lugar y tras asegurarnos de tener la versión adecuada de node indicada en la sección Lenguaje y plataforma lanzamos:
`npm install`

En segundo lugar se puede lanzar el comando de creación de base de datos:
`npx prisma migrate dev` la consola solicitará un nombre para la migración. Puedes escribir *initial* como nombre de la primera migración.

Si diese algún problema, eliminar el directorio *./prisma/migrations* y el archivo *prisma/dev.db* y volver a lanzar el comando anterior.

La consola debería mostrar algo similar a la siguiente imagen:
![Prisma Migrations](/extras/migrations_screenshot.png "Migrations Prisma Done!")

Si se quiere lanzar la solución se puede lanzar: 
`npm run start:dev`

Con el parámetro dev se activan más trazas en la consola.

## Testing
Tal como solicita la prueba, podría esta tentado de hacer un test unitario sobre el controlador, pero la capa HTTP de red no quedaría probada, por eso se crea un test e2e en lugar de un unitario sobre los endpoints responsables de los precios

Se pueden lanzar con el siguiente comando (siempre y cuando se hayan lanzado los comandos previamente descritos para la instalación):
`npm run test:e2e`

El sistema lanzará los tests mostrando un resultado final similar a este si todo ha ido correctamente:

![e2e tests](/extras/prices_endpoint_tests.png "Pruebas e2e")

## Extras 
Se adjunta colección de postman con un ejemplo de petición al endpoint creado en el archivo *./extras/inditex_nestjs.postman_collection.json*

