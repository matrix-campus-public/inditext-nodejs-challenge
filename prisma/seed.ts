/* eslint-disable prettier/prettier */
import { PrismaClient, Prisma } from '@prisma/client';
import { parseISO } from 'date-fns';

const prisma = new PrismaClient();

const brandsData: Prisma.brandsCreateInput[] = [
  {
    name: 'ZARA',
  },
];

const pricesData: Prisma.pricesCreateInput[] = [
  {
    product_id: 35455,
    brand_id: 1,
    price_list: 1,
    start_date: parseISO('2020-06-14T00:00:00.000Z'),
    end_date: parseISO('2020-12-31T23:59:59.000Z'),
    priority: 0,
    price: 35.50,
    currency: 'EUR',
  },
  {
    product_id: 35455,
    brand_id: 1,
    price_list: 2,
    start_date: parseISO('2020-06-14T15:00:00.000Z'),
    end_date: parseISO('2020-06-14T18:30:00.000Z'),
    priority: 1,
    price: 25.45,
    currency: 'EUR',
  },
  {
    product_id: 35455,
    brand_id: 1,
    price_list: 3,
    start_date: parseISO('2020-06-15T00:00:00.000Z'),
    end_date: parseISO('2020-06-15T11:00:00.000Z'),
    priority: 1,
    price: 30.50,
    currency: 'EUR',
  },
  {
    product_id: 35455,
    brand_id: 1,
    price_list: 4,
    start_date: parseISO('2020-06-16T00:00:00.000Z'),
    end_date: parseISO('2020-12-31T23:59:59.000Z'),
    priority: 1,
    price: 38.95,
    currency: 'EUR',
  },
];

async function main() {
  console.log(`Start seeding ...`);
  for (const brandData of brandsData) {
    const brand = await prisma.brands.create({
      data: brandData,
    });
    console.log(`Created brand with id: ${brand.id}`);
  }

  for (const priceData of pricesData) {
    const price = await prisma.prices.create({
      data: priceData,
    });
    console.log(`Created price with id: ${price.id}`);
  }
  console.log(`Seeding finished.`);
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
