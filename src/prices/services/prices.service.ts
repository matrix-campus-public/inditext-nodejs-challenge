/* eslint-disable prettier/prettier */
import { PriceDTO } from '../dtos/PriceDTO';
import { MapperService } from './mapper/mapper.service';
import { Injectable } from '@nestjs/common';
import { QueryPrice } from '../models/QueryPrice';
import { PrismaClient } from '@prisma/client'
import { parseISO } from 'date-fns';

@Injectable()
export class PricesService {

  private prismaClient: PrismaClient = new PrismaClient({    
    log: [
      {
        emit: 'event',
        level: 'query',
      },
      {
        emit: 'stdout',
        level: 'error',
      },
      {
        emit: 'stdout',
        level: 'info',
      },
      {
        emit: 'stdout',
        level: 'warn',
      },
    ],
  });

  constructor(private mapperService: MapperService){}

  query(query: QueryPrice): Promise<PriceDTO> {   
    return new Promise(async (resolve) => {      
      const price = await this.prismaClient.prices.findFirstOrThrow({
        where: {
          AND:{
            product_id:{
              equals: query.product_id
            },
            brand_id:{
              equals: query.brand_id
            },
            start_date:{
              lte: parseISO(query.date)
            },
            end_date:{
              gte: parseISO(query.date)
            }
          }
        },
        orderBy:{
          priority: 'desc'
        }      
      }); 
      if(price === undefined || price === null){
        throw new Error("price not found");
      }    
      resolve(this.mapperService.priceToDTO(price));
    })
  } 

}
