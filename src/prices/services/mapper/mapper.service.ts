import { prices } from '@prisma/client';
import { Injectable } from '@nestjs/common';
import { PriceDTO } from 'src/prices/dtos/PriceDTO';

@Injectable()
export class MapperService {
  priceToDTO(priceEntity: prices): PriceDTO {
    const { product_id, brand_id, price_list, start_date, end_date, price } = priceEntity;
    const dto: PriceDTO = { product_id, brand_id, price_list, start_date, end_date, price };
    return dto;
  }
}
