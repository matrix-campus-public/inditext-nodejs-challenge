/* eslint-disable prettier/prettier */
import { PricesService } from './../services/prices.service';
import { Body, Controller, HttpStatus, Post, Res } from '@nestjs/common';
import { QueryPrice } from '../models/QueryPrice';
import { PriceDTO } from '../dtos/PriceDTO';

@Controller('prices')
export class PricesController {

  constructor(private priceService: PricesService){}
  
  @Post('query')
  async get(@Body() query: QueryPrice, @Res() response) {
    try {      
      const price: PriceDTO = await this.priceService.query(query);
      return response.status(HttpStatus.OK).send(price);
    } catch (error) {
      return response.status(HttpStatus.UNPROCESSABLE_ENTITY).send({error: error.message})
    }
  }

}
