export interface PriceDTO {
  product_id: number;
  brand_id: number;
  price_list: number;
  start_date: Date;
  end_date: Date;
  price: number;
}
