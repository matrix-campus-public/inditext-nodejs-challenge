/* eslint-disable prettier/prettier */
import { IsNotEmpty } from 'class-validator';
export class QueryPrice {
  @IsNotEmpty()  
  date: string;

  @IsNotEmpty()
  product_id: number;

  @IsNotEmpty()
  brand_id: number;
}
