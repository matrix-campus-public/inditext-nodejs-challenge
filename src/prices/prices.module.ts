import { Module } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { PricesController } from './controllers/prices.controller';
import { MapperService } from './services/mapper/mapper.service';
import { PricesService } from './services/prices.service';

@Module({
  controllers: [PricesController],
  providers: [PricesService, MapperService, PrismaClient],
})
export class PricesModule {}
