import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { PricesModule } from './../src/prices/prices.module';
import { PricesService } from './../src/prices/services/prices.service';
import { INestApplication } from '@nestjs/common';
import { PriceDTO } from '../src/prices/dtos/PriceDTO';

describe('Prices e2e Test queries', () => {
  let app: INestApplication;  

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [PricesModule],
    })
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/POST Scenario 1: día 14 a las 10:00 deben ser 35.50€ `, () => {
    return request(app.getHttpServer())
      .post('/prices/query')
      .send({
        product_id: 35455,
        brand_id: 1,
        date: "2020-06-14T10:00:00.000Z"
       })
      .set('Accept', 'application/json')      
      .expect(200)
      .expect({
            product_id: 35455,
            brand_id: 1,
            price_list: 1,
            start_date: "2020-06-14T00:00:00.000Z",
            end_date: "2020-12-31T23:59:59.000Z",
            price: 35.5
        });
  });

  it(`/POST Scenario 2: día 14 a las 16:00 deben ser 25.45€`, () => {
    return request(app.getHttpServer())
      .post('/prices/query')
      .send({
        product_id: 35455,
        brand_id: 1,
        date: "2020-06-14T16:00:00.000Z"
       })
      .set('Accept', 'application/json')      
      .expect(200)
      .expect({
            product_id: 35455,
            brand_id: 1,
            price_list: 2,
            start_date: "2020-06-14T15:00:00.000Z",
            end_date: "2020-06-14T18:30:00.000Z",
            price: 25.45
        });
  });

  it(`/POST Scenario 3: día 14 a las 21:00 deben ser 35.50€`, () => {
    return request(app.getHttpServer())
      .post('/prices/query')
      .send({
        product_id: 35455,
        brand_id: 1,
        date: "2020-06-14T21:00:00.000Z"
       })
      .set('Accept', 'application/json')      
      .expect(200)
        .expect({
            product_id: 35455,
            brand_id: 1,
            price_list: 1,
            start_date: "2020-06-14T00:00:00.000Z",
            end_date: "2020-12-31T23:59:59.000Z",
            price: 35.5
        });
  });

  it(`/POST Scenario 4: día 15 a las 10:00 deben ser 30.50€`, () => {
    return request(app.getHttpServer())
      .post('/prices/query')
      .send({
        product_id: 35455,
        brand_id: 1,
        date: "2020-06-15T10:00:00.000Z"
       })
      .set('Accept', 'application/json')      
      .expect(200)
      .expect({
        product_id: 35455,
        brand_id: 1,
        price_list: 3,
        start_date: "2020-06-15T00:00:00.000Z",
        end_date: "2020-06-15T11:00:00.000Z",
        price: 30.5
    });
  });

  it(`/POST Scenario 5: día 16 a las 21.00 deben ser 38.95€`, () => {
    return request(app.getHttpServer())
      .post('/prices/query')
      .send({
        product_id: 35455,
        brand_id: 1,
        date: "2020-06-16T21:00:00.000Z"
       })
      .set('Accept', 'application/json')      
      .expect(200)
      .expect({
        product_id: 35455,
        brand_id: 1,
        price_list: 4,
        start_date: "2020-06-16T00:00:00.000Z",
        end_date: "2020-12-31T23:59:59.000Z",
        price: 38.95
    });
  });

  afterAll(async () => {
    await app.close();
  });
});